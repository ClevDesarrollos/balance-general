USE [URBANOVAERP]
GO

/****** Object:  Table [URBANOVA].[CLEV_TB_BAL_GRAL]    Script Date: 04/04/2019 08:24:14 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [URBANOVA].[CLEV_TB_BAL_GRAL](
	[CENTRO_COSTO] [varchar](25) NOT NULL,
	[DESCCCO] [varchar](200) NOT NULL,
	[NIVEL1] [varchar](50) NULL,
	[DESCNIVEL1] [varchar](200) NOT NULL,
	[NIVEL2] [varchar](50) NULL,
	[DESCNIVEL2] [varchar](200) NOT NULL,
	[NIVEL3] [varchar](50) NULL,
	[DESCNIVEL3] [varchar](200) NOT NULL,
	[NIVEL4] [varchar](50) NULL,
	[DESCNIVEL4] [varchar](200) NOT NULL,
	[NIVEL5] [varchar](50) NULL,
	[DESCNIVEL5] [varchar](200) NOT NULL,
	[NIVEL6] [varchar](50) NULL,
	[DESCNIVEL6] [varchar](200) NOT NULL,
	[NIVEL7] [varchar](50) NULL,
	[DESCNIVEL7] [varchar](200) NOT NULL,
	[NIVEL8] [varchar](50) NULL,
	[DESCNIVEL8] [varchar](200) NOT NULL,
	[NIVEL9] [varchar](50) NULL,
	[DESCNIVEL9] [varchar](200) NOT NULL,
	[NIVEL10] [varchar](50) NULL,
	[DESCNIVEL10] [varchar](200) NOT NULL,
	[CABEZA] [varchar](50) NULL,
	[GRANDE] [varchar](50) NULL,
	[SLOCAL] [decimal](28, 8) NULL,
	[SDOLAR] [decimal](28, 8) NULL
) ON [PRIMARY]
GO



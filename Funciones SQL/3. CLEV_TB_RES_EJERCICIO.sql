USE [URBANOVAERP]
GO

/****** Object:  Table [URBANOVA].[CLEV_TB_RES_EJERCICIO]    Script Date: 04/04/2019 08:18:20 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [URBANOVA].[CLEV_TB_RES_EJERCICIO](
	[mes1] [decimal](20, 8) NULL,
	[mes2] [decimal](20, 8) NULL,
	[mes3] [decimal](20, 8) NULL,
	[mes4] [decimal](20, 8) NULL,
	[mes5] [decimal](20, 8) NULL,
	[mes6] [decimal](20, 8) NULL,
	[mes7] [decimal](20, 8) NULL,
	[mes8] [decimal](20, 8) NULL,
	[mes9] [decimal](20, 8) NULL,
	[mes10] [decimal](20, 8) NULL,
	[mes11] [decimal](20, 8) NULL,
	[mes12] [decimal](20, 8) NULL
) ON [PRIMARY]
GO



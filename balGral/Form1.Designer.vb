﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Principal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Principal))
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.dateFechaFin = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnReporte2 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblError = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.radioAmbos2 = New System.Windows.Forms.RadioButton()
        Me.radioBolivianos = New System.Windows.Forms.RadioButton()
        Me.radioDolares = New System.Windows.Forms.RadioButton()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.checkCco = New System.Windows.Forms.CheckBox()
        Me.checkUtilidad = New System.Windows.Forms.CheckBox()
        Me.checkDiario = New System.Windows.Forms.CheckBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtUsuario = New System.Windows.Forms.TextBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.checkNiveles = New System.Windows.Forms.CheckedListBox()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Controls.Add(Me.TextBox1)
        Me.GroupBox4.Controls.Add(Me.dateFechaFin)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(154, Byte), Integer))
        Me.GroupBox4.Location = New System.Drawing.Point(16, 64)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox4.Size = New System.Drawing.Size(261, 199)
        Me.GroupBox4.TabIndex = 1
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Fecha"
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 13.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(27, 75)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 25)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Desde: "
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(105, 70)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(132, 30)
        Me.TextBox1.TabIndex = 2
        '
        'dateFechaFin
        '
        Me.dateFechaFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.dateFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateFechaFin.Location = New System.Drawing.Point(105, 111)
        Me.dateFechaFin.Margin = New System.Windows.Forms.Padding(4)
        Me.dateFechaFin.MinDate = New Date(2012, 12, 1, 0, 0, 0, 0)
        Me.dateFechaFin.Name = "dateFechaFin"
        Me.dateFechaFin.Size = New System.Drawing.Size(132, 29)
        Me.dateFechaFin.TabIndex = 3
        Me.dateFechaFin.Value = New Date(2014, 9, 23, 12, 34, 25, 0)
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Arial Narrow", 13.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(27, 111)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 25)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Hasta: "
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(154, Byte), Integer))
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Teal
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Arial", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button1.Location = New System.Drawing.Point(491, 270)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Button1.Name = "Button1"
        Me.Button1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Button1.Size = New System.Drawing.Size(182, 39)
        Me.Button1.TabIndex = 15
        Me.Button1.Text = "SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnReporte2
        '
        Me.btnReporte2.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(154, Byte), Integer))
        Me.btnReporte2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnReporte2.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan
        Me.btnReporte2.FlatAppearance.BorderSize = 0
        Me.btnReporte2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Teal
        Me.btnReporte2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.btnReporte2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReporte2.Font = New System.Drawing.Font("Arial", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReporte2.ForeColor = System.Drawing.Color.White
        Me.btnReporte2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnReporte2.Location = New System.Drawing.Point(303, 270)
        Me.btnReporte2.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnReporte2.Name = "btnReporte2"
        Me.btnReporte2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnReporte2.Size = New System.Drawing.Size(182, 39)
        Me.btnReporte2.TabIndex = 14
        Me.btnReporte2.Text = "REPORTE"
        Me.btnReporte2.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Arial Narrow", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(0, 18)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(940, 44)
        Me.Label2.TabIndex = 60
        Me.Label2.Text = "BALANCE GENERAL"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblError
        '
        Me.lblError.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblError.ForeColor = System.Drawing.Color.Red
        Me.lblError.Location = New System.Drawing.Point(7, 314)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(922, 50)
        Me.lblError.TabIndex = 61
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.radioAmbos2)
        Me.GroupBox1.Controls.Add(Me.radioBolivianos)
        Me.GroupBox1.Controls.Add(Me.radioDolares)
        Me.GroupBox1.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(154, Byte), Integer))
        Me.GroupBox1.Location = New System.Drawing.Point(283, 64)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox1.Size = New System.Drawing.Size(161, 199)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Moneda"
        '
        'radioAmbos2
        '
        Me.radioAmbos2.AutoSize = True
        Me.radioAmbos2.Font = New System.Drawing.Font("Arial Narrow", 13.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radioAmbos2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.radioAmbos2.Location = New System.Drawing.Point(22, 123)
        Me.radioAmbos2.Margin = New System.Windows.Forms.Padding(4)
        Me.radioAmbos2.Name = "radioAmbos2"
        Me.radioAmbos2.Size = New System.Drawing.Size(94, 30)
        Me.radioAmbos2.TabIndex = 7
        Me.radioAmbos2.Text = "Ambos"
        Me.radioAmbos2.UseVisualStyleBackColor = True
        '
        'radioBolivianos
        '
        Me.radioBolivianos.AutoSize = True
        Me.radioBolivianos.Checked = True
        Me.radioBolivianos.Font = New System.Drawing.Font("Arial Narrow", 13.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radioBolivianos.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.radioBolivianos.Location = New System.Drawing.Point(23, 48)
        Me.radioBolivianos.Margin = New System.Windows.Forms.Padding(4)
        Me.radioBolivianos.Name = "radioBolivianos"
        Me.radioBolivianos.Size = New System.Drawing.Size(124, 30)
        Me.radioBolivianos.TabIndex = 5
        Me.radioBolivianos.TabStop = True
        Me.radioBolivianos.Text = "Bolivianos"
        Me.radioBolivianos.UseVisualStyleBackColor = True
        '
        'radioDolares
        '
        Me.radioDolares.AutoSize = True
        Me.radioDolares.Font = New System.Drawing.Font("Arial Narrow", 13.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radioDolares.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.radioDolares.Location = New System.Drawing.Point(22, 85)
        Me.radioDolares.Margin = New System.Windows.Forms.Padding(4)
        Me.radioDolares.Name = "radioDolares"
        Me.radioDolares.Size = New System.Drawing.Size(99, 30)
        Me.radioDolares.TabIndex = 6
        Me.radioDolares.Text = "Dólares"
        Me.radioDolares.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.checkCco)
        Me.GroupBox3.Controls.Add(Me.checkUtilidad)
        Me.GroupBox3.Controls.Add(Me.checkDiario)
        Me.GroupBox3.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(154, Byte), Integer))
        Me.GroupBox3.Location = New System.Drawing.Point(450, 66)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox3.Size = New System.Drawing.Size(240, 197)
        Me.GroupBox3.TabIndex = 8
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Opciones"
        '
        'checkCco
        '
        Me.checkCco.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.checkCco.ForeColor = System.Drawing.SystemColors.WindowText
        Me.checkCco.Location = New System.Drawing.Point(7, 122)
        Me.checkCco.Name = "checkCco"
        Me.checkCco.Size = New System.Drawing.Size(203, 29)
        Me.checkCco.TabIndex = 11
        Me.checkCco.Text = "Incluir Centros de Costo"
        Me.checkCco.UseVisualStyleBackColor = True
        '
        'checkUtilidad
        '
        Me.checkUtilidad.Checked = True
        Me.checkUtilidad.CheckState = System.Windows.Forms.CheckState.Checked
        Me.checkUtilidad.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.checkUtilidad.ForeColor = System.Drawing.Color.Black
        Me.checkUtilidad.Location = New System.Drawing.Point(7, 88)
        Me.checkUtilidad.Name = "checkUtilidad"
        Me.checkUtilidad.Size = New System.Drawing.Size(151, 27)
        Me.checkUtilidad.TabIndex = 10
        Me.checkUtilidad.Text = "Calcular Utilidad"
        Me.checkUtilidad.UseVisualStyleBackColor = True
        '
        'checkDiario
        '
        Me.checkDiario.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.checkDiario.ForeColor = System.Drawing.Color.Black
        Me.checkDiario.Location = New System.Drawing.Point(6, 42)
        Me.checkDiario.Name = "checkDiario"
        Me.checkDiario.Size = New System.Drawing.Size(228, 45)
        Me.checkDiario.TabIndex = 9
        Me.checkDiario.Text = "Incluir transacciones del Diario"
        Me.checkDiario.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.txtUsuario)
        Me.GroupBox5.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(554, 1)
        Me.GroupBox5.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox5.Size = New System.Drawing.Size(216, 61)
        Me.GroupBox5.TabIndex = 18
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "USUARIO"
        Me.GroupBox5.Visible = False
        '
        'txtUsuario
        '
        Me.txtUsuario.Enabled = False
        Me.txtUsuario.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUsuario.Location = New System.Drawing.Point(8, 23)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(204, 30)
        Me.txtUsuario.TabIndex = 0
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.checkNiveles)
        Me.GroupBox6.Font = New System.Drawing.Font("Arial Narrow", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(154, Byte), Integer))
        Me.GroupBox6.Location = New System.Drawing.Point(696, 66)
        Me.GroupBox6.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox6.Size = New System.Drawing.Size(233, 197)
        Me.GroupBox6.TabIndex = 12
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Nivel de Análisis"
        '
        'checkNiveles
        '
        Me.checkNiveles.FormattingEnabled = True
        Me.checkNiveles.Location = New System.Drawing.Point(6, 29)
        Me.checkNiveles.Name = "checkNiveles"
        Me.checkNiveles.Size = New System.Drawing.Size(221, 158)
        Me.checkNiveles.TabIndex = 13
        '
        'Principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClientSize = New System.Drawing.Size(941, 371)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnReporte2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Principal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Balance General"
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Public WithEvents dateFechaFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnReporte2 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents radioAmbos2 As System.Windows.Forms.RadioButton
    Friend WithEvents radioBolivianos As System.Windows.Forms.RadioButton
    Friend WithEvents radioDolares As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents checkDiario As System.Windows.Forms.CheckBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents checkUtilidad As System.Windows.Forms.CheckBox
    Friend WithEvents checkCco As CheckBox
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents checkNiveles As CheckedListBox
End Class

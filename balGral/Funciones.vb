﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Xml
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography

Module Funciones


#Region "CIFRADO"
    Public Function Encriptar(ByVal Input As String) As String

        Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("softland") 'La clave debe ser de 8 caracteres
        Dim EncryptionKey() As Byte = Convert.FromBase64String("CleverLimitadaLaPazBolivia2015V1") 'No se puede alterar la cantidad de caracteres pero si la clave
        Dim buffer() As Byte = Encoding.UTF8.GetBytes(Input)
        Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
        des.Key = EncryptionKey
        des.IV = IV

        Return Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length()))

    End Function

    Public Function Desencriptar(ByVal Input As String) As String

        Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("softland") 'La clave debe ser de 8 caracteres
        Dim EncryptionKey() As Byte = Convert.FromBase64String("CleverLimitadaLaPazBolivia2015V1") 'No se puede alterar la cantidad de caracteres pero si la clave
        Try
            Dim buffer() As Byte = Convert.FromBase64String(Input)
            Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
            des.Key = EncryptionKey
            des.IV = IV
            Return Encoding.UTF8.GetString(des.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length()))

        Catch ex As Exception
            'Console.WriteLine(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
            'MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
            Return Input

        End Try


    End Function
#End Region



    Public Function obtenerDatos(ByVal valor As Integer) As String
        Dim esquema As String = ""
        AbrirXml()
        Try
            NodeList = Xml.SelectNodes("/Documento/DatosReporte")
            If valor = 1 Then
                esquema = Desencriptar(NodeList(0).Attributes.GetNamedItem("Dato1").Value)
            End If
            If valor = 2 Then
                esquema = Desencriptar(NodeList(0).Attributes.GetNamedItem("Dato2").Value)
            End If
            If valor = 3 Then
                esquema = Desencriptar(NodeList(0).Attributes.GetNamedItem("Dato3").Value)
            End If
            If valor = 4 Then
                esquema = Desencriptar(NodeList(0).Attributes.GetNamedItem("Dato4").Value)
            End If


        Catch ex As Exception
            'Console.WriteLine(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()
        Return esquema
    End Function

#Region "Variables"
    Public conexion As SqlConnection
    Public CN As String = Desencriptar(ObtenerCadenaConexion())
    Dim Xml As XmlDocument
    Dim NodeList, NodeList1, NodeListR, NodeListBoleta As XmlNodeList
    Public enunciado As SqlCommand
    Public respuesta As SqlDataReader
    Public Con As String

    Public plantilla As String
    Public filadatos As String
    Public columnadatos As String
    Public extension As String
#End Region

#Region "Conexión a base de datos"

    Sub conectar()
        Try
            conexion = New SqlConnection(CN)
            conexion.Open()

            'MsgBox("Conectado")

        Catch ex As Exception
            MsgBox("No se conecto" + ex.ToString)
        End Try
    End Sub
    Public Sub Desconectar()
        conexion.Close()
        conexion = Nothing
    End Sub

    Sub llenarComboBox(ByVal cb As ComboBox, ByVal query As String, ByVal columna As String)
        conectar()
        Try
            enunciado = New SqlCommand(query, conexion)
            respuesta = enunciado.ExecuteReader
            While respuesta.Read
                cb.Items.Add(respuesta.Item(columna))
            End While
            respuesta.Close()
        Catch ex As Exception
        End Try
        Desconectar()
    End Sub

    Public Function DevolverDatoQuery(ByVal SQL As String) As String
        conectar()
        enunciado = New SqlCommand(SQL, conexion)
        Dim Devolver As String = ""
        Devolver = enunciado.ExecuteScalar()
        enunciado = Nothing
        Desconectar()
        Return Devolver

    End Function

    Sub ejecutarComandosSQL(ByVal query As String)
        Try
            conectar()
            enunciado = New SqlCommand(query, conexion)
            enunciado.CommandTimeout = 6000
            Dim t As Integer = enunciado.ExecuteNonQuery()
            Desconectar()
        Catch ex As Exception
            MessageBox.Show(" *** No se pudo ejecutar la instrucción. *** " + ex.Message)
        End Try

    End Sub

#End Region


    Sub cargaTabla(ByVal SQL As String, ByVal dtgview As DataGridView)
        conexion = New SqlConnection
        conexion.ConnectionString = CN
        Dim micomando As New SqlCommand(SQL, conexion)
        Dim midataAdapter As New SqlDataAdapter
        conexion.Open()
        micomando.CommandTimeout = 9000
        midataAdapter.SelectCommand = micomando

        Dim midataset As New DataSet

        midataset.Clear()

        midataAdapter.Fill(midataset, "vista")

        dtgview.DataSource = midataset.Tables(0)

        conexion.Close()
    End Sub

#Region "Conexión XML"
    Public Sub AbrirXml()
        Xml = New XmlDocument()
        Try
            If My.Computer.FileSystem.FileExists("c:\WINDOWS\SYSTEM32\Config_balGral.xml") Then
                Xml.Load("c:\WINDOWS\SYSTEM32\Config_movContables.xml")
            Else
                If My.Computer.FileSystem.FileExists("c:\WINDOWS\Config_balGral.xml") Then
                    Xml.Load("c:\WINDOWS\Config_movContables.xml")
                Else
                    Xml.Load(My.Application.Info.DirectoryPath & "\Config_balGral.xml")
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
    End Sub
    Public Function obtenerUsuarioPassCR(ByVal valor As String) As String
        Dim esquema As String = ""
        AbrirXml()
        Try
            NodeList = Xml.SelectNodes("/Documento/CrystalReports")
            If valor = "User" Then
                esquema = Desencriptar(NodeList(0).Attributes.GetNamedItem("User").Value)
            Else
                esquema = Desencriptar(NodeList(0).Attributes.GetNamedItem("Pass").Value)
            End If

        Catch ex As Exception
            'Console.WriteLine(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()
        Return esquema
    End Function

    Public Function obtenerUsuario() As String
        Dim esquema As String = ""
        AbrirXml()
        Try
            NodeList = Xml.SelectNodes("/Documento/UsuarioImprime")

            esquema = NodeList(0).Attributes.GetNamedItem("Nombre").Value


        Catch ex As Exception
            'Console.WriteLine(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()
        Return esquema
    End Function

    Public Function obtenerDivisor() As String
        Dim esquema As String = ""
        AbrirXml()
        Try
            NodeList = Xml.SelectNodes("/Documento/CaracterDivisor")

            esquema = NodeList(0).Attributes.GetNamedItem("Nombre").Value


        Catch ex As Exception
            'Console.WriteLine(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()
        Return esquema
    End Function

    Public Sub CerrarXml()
        Xml = Nothing
    End Sub

    Public Function ObtenerNroConexion() As String
        Dim Nro As String = "100"
        AbrirXml()
        Try
            NodeList = Xml.SelectNodes("/Documento/Conexiones/ConexionActiva")
            Nro = NodeList(0).Attributes.GetNamedItem("NumeroConexion").Value
        Catch ex As Exception
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()
        Return Nro
    End Function

    Public Function ObtenerCadenaConexion() As String
        Dim Cadena As String = ""
        Dim Nro As String = ObtenerNroConexion()

        AbrirXml()
        Try
            NodeList = Xml.SelectNodes("/Documento/Conexiones/Conexion" + Nro)
            Cadena = NodeList(0).Attributes.GetNamedItem("Cadena").Value
        Catch ex As Exception
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()
        Return Cadena
    End Function



    Public Function ObtenerEsquema() As String
        Dim esquema As String = ""
        AbrirXml()
        Try
            NodeList = Xml.SelectNodes("/Documento/EsquemaBaseDeDatos")
            esquema = NodeList(0).Attributes.GetNamedItem("Esquema").Value
        Catch ex As Exception
            'Console.WriteLine(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()
        Return esquema
    End Function

    Public Function ObtenerRutaLogoReportes(ByVal valor As String) As String
        Dim esquema As String = ""
        AbrirXml()
        Try
            NodeList = Xml.SelectNodes("/Documento/RutaReporte")
            If valor = "ruta" Then
                esquema = NodeList(0).Attributes.GetNamedItem("Ruta").Value
            End If
            If valor = "logo" Then
                esquema = NodeList(0).Attributes.GetNamedItem("Logo").Value
            End If
        Catch ex As Exception
            'Console.WriteLine(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()
        Return esquema
    End Function



#End Region
End Module

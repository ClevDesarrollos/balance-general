﻿Public Class Principal
    Dim query As String
    Dim esquema As String = ObtenerEsquema()
    Dim caracterDivisor As String = obtenerDivisor()

    Private Sub Principal_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        txtUsuario.Text = obtenerUsuario()
        dateFechaFin.Value = Now.Date
        llenarCheckNiveles()

    End Sub

   

    Private Sub dateFechaFin_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dateFechaFin.ValueChanged
       
            btnReporte2.Enabled = True
            lblError.Text = "" 

    End Sub

    Private Sub btnReporte2_Click(sender As System.Object, e As System.EventArgs) Handles btnReporte2.Click
        If txtUsuario.Text = "" Then
            lblError.Text = "Debe escribir un nombre de usuario para continuar."
        Else
            lblError.Text = ""
            Reporte.Show()
            Me.Hide()
        End If

       

    End Sub


    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Application.Exit()
    End Sub


#Region "Nivel de Análisis"
    'obtenemos la cantidad de niveles definidos para las cuentas contables
    Function obtenerCantNiveles() As Integer
        Dim a As Integer
        Dim niv As String
        query = " SELECT PATRON FROM " & esquema & ".GLOBALES_CG"
        niv = DevolverDatoQuery(query)
        a = niv.Split(caracterDivisor).Length
        Return a
    End Function

    'obtenemos los niveles definidos para las cuentas contables
    Function mostrarNiveles(ByVal cant As Integer) As String
        Dim tam As Integer
        Dim niv As String()
        Dim niveles As String = ""
        query = " SELECT PATRON FROM " & esquema & ".GLOBALES_CG"
        niv = DevolverDatoQuery(query).Split(caracterDivisor)
        tam = niv.Length
        For i As Integer = 0 To tam - 1 Step 1
            If i = 0 Then
                niveles += niv(i)
            Else
                If i <= cant Then
                    niveles += caracterDivisor + niv(i)
                Else
                    niveles += caracterDivisor + niv(i).Replace(niv(i).Substring(0, 1), "0")
                End If
            End If
        Next
        Return niveles
    End Function
    Sub llenarCheckNiveles()
        Dim cant As Integer = obtenerCantNiveles()
        Dim i As Integer
        For i = 0 To cant - 1
            checkNiveles.Items.Add(mostrarNiveles(i))
            checkNiveles.SetItemChecked(i, True)
        Next

    End Sub
#End Region

End Class

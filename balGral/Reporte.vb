﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine


Public Class Reporte
    Dim rpt As ReportDocument = New ReportDocument()
    Dim ruta As String = ObtenerRutaLogoReportes("ruta")
    Dim esquema As String = ObtenerEsquema()
    Dim query As String
    Private Sub Reporte_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load, Button1.Click

        'EJECUTAMOS EL PROCEDIMIENTO ALMACENADO PARA EL AÑO SELECCIONADO, EL PROCESO SE EJECUTARÁ PARA TODOS LOS MESES DEL AÑO SELECCIONADO, POR ESO PONEMOS 12 EN MES
        Dim fechaIni As String
        Dim fechaFin As String
        Dim mayor As String
        Dim resEje As String
        Dim LogoCompania As String = ObtenerRutaLogoReportes("logo")

        Dim N2 As String = "0"
        Dim N3 As String = "0"
        Dim N4 As String = "0"
        Dim N5 As String = "0"
        Dim cc As String = "0"

        Dim dato1 As String = obtenerDatos(1)
        Dim dato2 As String = obtenerDatos(2)
        Dim dato3 As String = obtenerDatos(3)
        Dim dato4 As String = obtenerDatos(4)

        Dim moneda As String = ""
        Dim infoMoneda As String = ""
        fechaIni = DevolverDatoQuery("SELECT TOP 1  Convert(Char(10), FECHA, 112) FECHA  FROM (SELECT FECHA FROM " & esquema & ".ASIENTO_MAYORIZADO UNION ALL SELECT FECHA FROM " & esquema & ".ASIENTO_DE_DIARIO)VISTA ORDER BY FECHA ASC") & " 00:00:00"
        fechaFin = Principal.dateFechaFin.Value.ToString("yyyyMMdd") & " 23:59:59"

        If Principal.checkDiario.Checked = True Then
            mayor = "A"
        Else
            mayor = "M"
        End If

        If Principal.checkUtilidad.Checked = True Then
            resEje = "0"
        Else
            resEje = "1"
        End If




        If Principal.checkCco.Checked = True Then
            cc = "0"
        Else
            cc = "1"
        End If

        If Principal.radioBolivianos.Checked = True Then
            moneda = "N"
            infoMoneda = "Expresado en Bolivianos"
        Else
            If Principal.radioDolares.Checked = True Then
                moneda = "E"
                infoMoneda = " Expresado en Dólares"
            Else
                moneda = "A"
                infoMoneda = " Expresado en Bolivianos y Dólares"
            End If
        End If



        query = " EXEC " & esquema & ".CLEV_SP_BAL_GRAL"
        query += " @FECHAFIN = '" & fechaFin & "', "
        query += " @MAYOR = '" & mayor & "'"

        ejecutarComandosSQL(query)


        rpt.Load(ruta + "BalGral.rpt")

        '--------------------------------------------------
        Dim CN As String = Desencriptar(ObtenerCadenaConexion())
        conexion = New SqlConnection
        conexion.ConnectionString = CN
        query = " SELECT * FROM " & esquema & ".CLEV_TB_BAL_GRAL "
        Dim micomando As New SqlCommand(query, conexion)
        Dim midataAdapter As New SqlDataAdapter
        conexion.Open()
        micomando.CommandTimeout = 9000
        midataAdapter.SelectCommand = micomando
        Dim midataset As New DataSet
        midataset.Clear()
        midataAdapter.Fill(midataset, "Items")
        conexion.Close()
        rpt.SetDataSource(midataset)
        '--------------------------------------------------



        rpt.SetDatabaseLogon(obtenerUsuarioPassCR("User"), obtenerUsuarioPassCR("Pass"))

        rpt.SetParameterValue("@logoCompania", LogoCompania)
        rpt.SetParameterValue("@dato1", dato1)
        rpt.SetParameterValue("@dato2", dato2)
        rpt.SetParameterValue("@dato3", dato3)
        rpt.SetParameterValue("@dato4", dato4)
        rpt.SetParameterValue("@subtitulo", "AL " + Principal.dateFechaFin.Value + "" + vbCr + infoMoneda)
        rpt.SetParameterValue("@usuario", Principal.txtUsuario.Text)
        rpt.SetParameterValue("@moneda", moneda)
        Dim i As Integer
        For i = 0 To 9
            Try
                If (Principal.checkNiveles.GetItemChecked(i)) Then
                    rpt.SetParameterValue("@n" & (i + 1).ToString, 0)
                Else
                    rpt.SetParameterValue("@n" & (i + 1).ToString, 1)
                End If
            Catch ex As Exception
                rpt.SetParameterValue("@n" & (i + 1).ToString, 1)
            End Try
        Next
        rpt.SetParameterValue("@cc", cc)

        rpt.SetParameterValue("@resEjercicio", resEje)

        crvPrincipal.EnableRefresh = True
        crvPrincipal.ReuseParameterValuesOnRefresh = True
        crvPrincipal.ReportSource = rpt
        crvPrincipal.Show()
        crvPrincipal.Zoom(150)

    End Sub

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles Me.FormClosing

        ' Estos son los valores posibles
        Select Case e.CloseReason
            Case CloseReason.ApplicationExitCall
            Case CloseReason.FormOwnerClosing
            Case CloseReason.MdiFormClosing
            Case CloseReason.None
            Case CloseReason.TaskManagerClosing
            Case CloseReason.UserClosing
                Principal.Visible = True
            Case CloseReason.WindowsShutDown
        End Select
    End Sub

End Class